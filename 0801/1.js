var SeleccionFutbol=function(datos){ /* Creamos una clase "SeleccionFutbol" */
	this.id=0;
	this.nombre="";
	this.apellidos="";
	this.edad=0;

	this.concentrarse=function(){
		console.log("Concentrandose");
	};
	
	this.viajar=function(){
		console.log("Viajando");
	};
	/* Getters y Setters */
	this.getId=function(){
		return this.id;
	}
	this.setId=function(argumento){
		this.id=argumento||0;
	}
	this.getNombre=function(){
		return this.nombre;
	}
	this.setNombre=function(argumento){
		this.nombre=argumento||"no tiene";
	}
	this.getApellidos=function(){
		return this.apellidos;
	}
	this.setApellidos=function(argumento){
		this.apellidos=argumento||"no tiene";
	}
	this.getEdad=function(){
		return this.edad;
	}
	this.setEdad=function(argumento){
		this.edad=argumento||0;
	}
	/* fin Getters y Setters */
	/* Constructor */
	this.seleccionFutbol=function(v){
		this.setId(v.id);
		this.setNombre(v.nombre);
		this.setApellidos(v.apellidos);
		this.setEdad(v.edad);
	}
	/* fin constructor */
	this.seleccionFutbol(datos); /* llamamos a la clase SeleccionFutbol */
}

var Futbolista=function(datos){ /* Creamos una clase "Futbolista" */
	this.dorsal=0;
	this.demarcacion="";

	this.jugarPartido=function(){
		console.log("Jugando");
	};
	
	this.entrenar=function(){
		console.log("Entrenando");
	};
	/* Getters y Setters */
	this.getDorsal=function(){
		return this.dorsal;
	}
	this.setDorsal=function(argumento){
		this.dorsal=argumento||0;
	}
	this.getDemarcacion=function(){
		return this.demarcacion;
	}
	this.setDemarcacion=function(argumento){
		this.demarcacion=argumento||"no tiene";
	}
	/* fin Getters y Setters */
	/* Constructor */
	this.futbolista=function(v){
		this.setDorsal(v.dorsal);
		this.setDemarcacion(v.demarcacion);
		this.seleccionFutbol(v);
	}
	/* fin constructor */
	this.futbolista(datos); /* llamamos a la clase Futbolista */
}

var Entrenador=function(datos){ /* Creamos una clase "Entrenador" */
	this.idFederacion="";

	this.dirigirPartido=function(){
		console.log("...No haciendo nada...");
	};
	
	this.dirigirEntrenamiento=function(){
		console.log("Chicos hoy vamos a practicar el piscinazo, que no nos pitan penaltis");
	};
	/* Getters y Setters */
	this.getIdFederacion=function(){
		return this.idFederacion;
	}
	this.setIdFederacion=function(argumento){
		this.idFederacion=argumento||"no tiene";
	}
	/* fin Getters y Setters */
	/* Constructor */
	this.entrenador=function(v){
		this.setIdFederacion(v.idFederacion);
		this.seleccionFutbol(v);
	}
	/* fin constructor */
	this.entrenador(datos); /* llamamos a la clase Entrenador */
}

var Masajista=function(datos){ /* Creamos una clase "Majasista" */
	this.titulacion="";
	this.aniosExperiencia=0;

	this.darMasaje=function(){
		console.log("Estas muy tenso, oh vaya un nudo");
	};
	/* Getters y Setters */
	this.getTitulacion=function(){
		return this.titulacion;
	}
	this.setTitulacion=function(argumento){
		this.titulacion=argumento||"no tiene";
	}
	this.getAniosExperiencia=function(){
		return this.aniosExperiencia;
	}
	this.setAniosExperiencia=function(argumento){
		this.aniosExperiencia=argumento||0;
	}
	/* fin Getters y Setters */
	/* Constructor */
	this.masajista=function(v){
		this.setTitulacion(v.titulacion);
		this.setAniosExperiencia(v.aniosExperiencia);
		this.seleccionFutbol(v);
	}
	/* fin constructor */
	this.masajista(datos); /* llamamos a la clase Futbolista */
}

Futbolista.prototype=new SeleccionFutbol({}); /* futbolista recibe la herencia de SeleccionFutbol */
Entrenador.prototype=new SeleccionFutbol({}); /* Entrenador recibe la herencia de SeleccionFutbol */
Masajista.prototype=new SeleccionFutbol({}); /* Masajista recibe la herencia de SeleccionFutbol */

/* instanciamos */
var objeto1=new Futbolista({
	id:101,
	nombre:"Neymar",
	apellidos:"Jr",
	edad:26,
	dorsal:10,
	demarcacion:"delantero"
});

var objeto2=new Entrenador({
	id:201,
	nombre:"Vicente",
	apellidos:"Del Bosque",
	edad:67,
	idFederacion:"aaa"
});

var objeto3=new Masajista({
	id:301,
	nombre:"Paula",
	apellidos:"Jr",
	edad:28,
	titulacion:"Quiromasajista",
	aniosExperiencia:6

});
/* fin instancia*/

console.log(objeto1);
console.log(objeto2);
console.log(objeto3);
//commit
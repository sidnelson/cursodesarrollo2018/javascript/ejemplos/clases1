var Persona=function(datos){ /* Creamos una clase "Persona" */
	this.nombre="";
	this.direccion="";
	this.correoElectronico="";

	this.crear=function(){
		console.log("Creando");
	};
	this.borrar=function(){
		console.log("Esto no me gusta willy borrao :3");
	};
	this.enviarMensaje=function(){
		console.log("Hola muy buenas...");
	};
	/* Getters y Setters */
	this.getNombre=function(){
		return this.nombre;
	}
	this.setNombre=function(argumento){
		this.nombre=argumento||"no tiene";
	}
	this.getDireccion=function(){
		return this.direccion;
	}
	this.setDireccion=function(argumento){
		this.direccion=argumento||"no tiene";
	}
	this.getCorreoElectronico=function(){
		return this.correoElectronico;
	}
	this.setCorreoElectronico=function(argumento){
		this.correoElectronico=argumento||"no tiene";
	}
	/* fin Getters y Setters */
	/* Constructor */
	this.persona=function(v){
		this.setNombre(v.nombre);
		this.setDireccion(v.direccion);
		this.setCorreoElectronico(v.correoElectronico);
	}
	/* fin constructor */
	this.persona(datos); /* llamamos a la clase Persona */
}

var Cliente=function(datos){ /* Creamos una clase "Cliente" */
	this.numeroCliente=0;
	this.fechaAlta=0;

	this.verFechaAlta=function(){
		console.log("Te diste de alta el...");
	};
	/* Getters y Setters */
	this.getNumeroCliente=function(){
		return this.numeroCliente;
	}
	this.setNumeroCliente=function(argumento){
		this.numeroCliente=argumento||0;
	}
	this.getFechaAlta=function(){
		return this.fechaAlta;
	}
	this.setFechaAlta=function(argumento){
		this.fechaAlta=argumento||0;
	}
	/* fin Getters y Setters */
	/* Constructor */
	this.cliente=function(v){
		this.setNumeroCliente(v.numeroCliente);
		this.setFechaAlta(v.fechaAlta);
		this.persona(v);
	}
	/* fin constructor */
	this.cliente(datos); /* llamamos a la clase Cliente */
}

var Usuario=function(datos){ /* Creamos una clase "Usuario" */
	this.codigoUsuario=0;

	this.autorizar=function(){
		console.log("Autorizando");
	};

	/* Getters y Setters */
	this.getCodigoUsuario=function(){
		return this.codigoUsuario;
	}
	this.setCodigoUsuario=function(argumento){
		this.codigoUsuario=argumento||0;
	}
	/* fin Getters y Setters */
	/* Constructor */
	this.usuario=function(v){
		this.setCodigoUsuario(v.codigoUsuario);
		this.persona(v);
	}
	/* fin constructor */
	this.usuario(datos); /* llamamos a la clase Persona */
}

Cliente.prototype=new Persona({}); /* Cliente recibe la herencia de Persona */
Usuario.prototype=new Persona({}); /* Usuario recibe la herencia de Persona */

var objeto1=new Cliente({
	nombre:"Jose",
	direccion:"Vazquez Rodriguez",
	correoElectronico:"joseVR2000@gmail.com",
	numeroCliente:1300,
	fechaAlta:"1/1/2018"
});

var objeto2=new Usuario({
	nombre:"Admin",
	direccion:"mi sitio",
	correoElectronico:"Admin@gmail.com",
	codigoUsuario:13
});


console.log(objeto1);
console.log(objeto2);